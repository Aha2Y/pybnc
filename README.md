##Intro
PyBNC is a python bouncer IRC bot.

It will connect to IRC and your bouncer at the same time.

So it can relay commands from IRC directly to your bouncer.

PyBNC will support ZNC, PsyBNC and Shroudbnc for now.

##Commands. 
- !help - Tells you all this in IRC.
- !uptime - Shows the uptime of your bouncer and the bot.
- !version - Returns the version of the bouncer.
- !rehash - Rehash the bouncer (admin only)
- !restart - Restart the bouncer (admin only)
- !global - Send a global to all users of the bouncer (admin only)
- !shutdown - Shutdowns the bouncer. (admin only)
 

