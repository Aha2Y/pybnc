#!/usr/bin/python
from ConfigParser import RawConfigParser as ConfParser
from optparse import OptionParser
from collections import deque
from time import sleep, time, gmtime, strftime
import ConfigParser
import datetime
import os
import sys
import socket
import codecs
from irc import *
from select import select


os.system('clear')
timestamp = '[' +'\x1b[32m' + datetime.datetime.now().strftime("%a %d, %H:%M:%S") + '\x1b[0m' + ']'
my = IRCbot()
start = time()

print('%s Starting Pybnc by Aha2Y. (Pid number: %s)' % (timestamp, os.getpid()))
try: 
   config = ConfigParser.RawConfigParser()
   config.readfp(codecs.open("config.ini", "r", "utf8"))
   chanQueue = deque() ; channels = []
   print('%s Successfully loaded config.ini' % (timestamp))
except IOError as config:
   print('%s [\x1b[31mError\x1b[0m] PyZNC Can\'t read config.ini...' % (timestamp))
   print('%s [\x1b[31mError\x1b[0m] Be sure you have the config file in the same dir as PyZNC is!' % (timestamp))
   sys.exit()
   
irc = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
bnc = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
print('%s Connecting to the IRC server (%s:%s)' % (timestamp, config.get('irc-connection', 'server'), config.getint('irc-connection', 'port')))
print('%s Connecting to the BNC server (%s:%s)' % (timestamp, config.get('bnc-connection', 'server'), config.getint('bnc-connection', 'port')))
irc.connect ((config.get('irc-connection', 'server'),config.getint('irc-connection', 'port')))
bnc.connect ((config.get('bnc-connection', 'server'),config.getint('bnc-connection', 'port')))
irc.send ('NICK %s\r\n' % config.get('irc-bot', 'nickname'))
bnc.send ('NICK %s\r\n' % config.get('irc-bot', 'nickname'))
irc.send ('USER %s %s pybnc :%s\r\n' % (config.get('irc-bot', 'nickname'), config.get('irc-bot', 'ident'), config.get('irc-bot', 'realname')))
bnc.send ('USER %s %s pybnc :%s\r\n' % (config.get('irc-bot', 'nickname'), config.get('irc-bot', 'ident'), config.get('irc-bot', 'realname')))

while 1:
    (rl, wl, xl) = select([irc, bnc], [], [])
    for sock in rl:
        data = readline(sock)
        raw = parse(data)
        if sock == bnc:
            if raw[0] == "PING":
               sock.send("PONG %s\r\n" % raw[1])
               print('%s Received ping from the bnc server, Sending pong.' % timestamp)
            print "[\x1b[32mbnc\x1b[0m]", raw
            if len(raw) > 3 and raw[3] == "*** You need to send your password. Try /quote PASS <username>:<password>":
               bnc.send('PASS %s \r\n' % config.get('bnc-connection', 'login'))
               print('%s Bouncer Connection established.' % timestamp)
            if raw[0] == "*status!znc@znc.in":
               if raw[1] == "PRIVMSG":
                  if "Running for" in raw[3]:
                     channel = chanQueue.popleft()
                     uptime = " ".join(raw[3:])
                     irc.send('PRIVMSG %s :The bouncer is %s\r\n' % (channel, uptime))
                  if "- http://znc.in" in raw[3]:
                     version = raw[3].strip('- http://znc.in')
                  if "IPv6:" in raw[3]:
                     channel = chanQueue.popleft()
                     version2 = " ".join(raw[3:])
                     irc.send('PRIVMSG %s :The bouncer is running: %s\r\n' % (channel, version))
                     irc.send('PRIVMSG %s :And the settings: %s\r\n' % (channel, version2))
        elif sock == irc:
            if raw[0] == "PING":
               sock.send("PONG %s\r\n" % raw[1])
               print('%s Received ping from the irc server, Sending pong.' % timestamp)
            print "[\x1b[31mirc\x1b[0m]", raw
            if raw[1] == "439" and raw[3] == "Please wait while we process your connection.":
               my.server = raw[0]
            if raw[1] == "376" and raw[3] == "End of /MOTD command.":
               irc.send('JOIN %s \r\n' % config.get('irc-bot', 'mainchan'))
               print('%s IRC Connection established.' % timestamp)
            if raw[0].startswith("NickServ!"):
               if raw[1] == "NOTICE" and "This nickname is registered" in raw[3]:
                  irc.send('PRIVMSG NickServ :identify %s \r\n' % config.get('irc-bot', 'nickpass'))
            if raw[1] == "PRIVMSG":
               my.sender = raw[0]
               my.nick = raw[0].split("!")[0]
               my.auth = data.split('@')[0][1:]
               if len(raw) > 3:
                  if raw[3] == "!uptime":
                     chanQueue.append(raw[2])
                     end = time()
                     uptime = end - start
                     bnc.send('PRIVMSG *status :uptime \r\n')
                     irc.send('PRIVMSG %s :The bot is running for %s \r\n' % (raw[2], duration_human(uptime)))
               if raw[3] == "!rehash":
                  try: 
                     if config.get('staff', my.auth) == 'admin':
                        chanQueue.append(raw[2])
                        bnc.send('PRIVMSG *status :rehash\r\n')
                        irc.send('PRIVMSG %s :Bouncer successfuly rehashed!\r\n' % raw[2])
                  except ConfigParser.NoOptionError:
                     irc.send('PRIVMSG %s :I don\'t recognize you. \r\n' % raw[2])
               if raw[3] == "!shutdown":
                  try: 
                     if config.get('staff', my.auth) == 'admin':
                        chanQueue.append(raw[2])
                        bnc.send('PRIVMSG *status :shutdown\r\n')
                  except ConfigParser.NoOptionError:
                     irc.send('PRIVMSG %s :I don\'t recognize you. \r\n' % raw[2])
               if raw[3] == "!restart":
                  try: 
                     if config.get('staff', my.auth) == 'admin':
                        chanQueue.append(raw[2])
                        bnc.send('PRIVMSG *status :reboot\r\n')
                        irc.send('PRIVMSG %s :Restarting server...\r\n' % raw[2])
                  except ConfigParser.NoOptionError:
                     irc.send('PRIVMSG %s :I don\'t recognize you. \r\n' % raw[2])
               if raw[3].startswith("!global "):
                  try: 
                     if config.get('staff', my.auth) == 'admin': 
                        globalmsg = raw[3].split(" ", 1)[1]
                        globalmessage = " ".join(raw[3:])
                        bnc.send('PRIVMSG *status :broadcast %s\r\n' % globalmsg)
                        irc.send('PRIVMSG %s :[Global] %s\r\n' % (raw[2], globalmsg))
                  except ConfigParser.NoOptionError:
                     irc.send('PRIVMSG %s :I don\'t recognize you. \r\n' % raw[2])
                  if raw[3] == "!version":
                     chanQueue.append(raw[2])
                     bnc.send('PRIVMSG *status :version \r\n')
                  if raw[3] == "!help": 
                     irc.send('PRIVMSG %s :Available commands: !version, !uptime, !whoami, !rehash, !restart, !shutdown, !global\r\n' % raw[2])
                     try: 
                        if config.get('staff', my.auth) == 'admin':
                           irc.send('PRIVMSG %s :You are authenticated as admin. \r\n' % raw[2])
                     except ConfigParser.NoOptionError:
                           irc.send('PRIVMSG %s :I don\'t recognize you. \r\n' % raw[2])
       
